import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { CartapiService } from '../services/cartapi.service'; // Import the CartapiService (assuming it's correct)

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productList: any;

  constructor(private api: ApiService, private cartApi: CartapiService) {}

  ngOnInit(): void {
    this.api.getProducts().subscribe((res) => {
      this.productList = res;

      this.productList.forEach((a: any) => {
        Object.assign(a, { quality: 1, total: a.price });
      });
    });
  }

  addtoCart(item: any) {
    this.cartApi.addToCart(item);
  }
}
